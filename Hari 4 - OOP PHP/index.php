<?php

    //require('animal.php');
    require('frog.php');
    require_once('monkey.php');
  
    
    $sheep = new animal("shaun");

    echo "Animal's Name : $sheep->name <br>"; 
    echo "Legs : $sheep->legs <br>"; 
    echo "Cold Blooded : $sheep->cold_blooded <br><br>"; 


    $kodok = new frog("buduk");

    echo "Animal's Name : $kodok->name <br>"; 
    echo "Legs : $kodok->legs <br>"; 
    echo "Cold Blooded : $kodok->cold_blooded <br>"; 
    $kodok->jump();
    echo "<br><br>";

    
    $sungkong = new ape("kera sakti");

    echo "Animal's Name : $sungkong->name <br>"; 
    echo "Legs : $sungkong->legs <br>"; 
    echo "Cold Blooded : $sungkong->cold_blooded <br>"; 
    $sungkong->yell();
   